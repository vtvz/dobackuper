FROM node:12.13.1

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn --frozen-lock

COPY src ./src

CMD [ "yarn", "start" ]
