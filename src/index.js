const k8s = require('@kubernetes/client-node');
const { DigitalOcean } = require('digitalocean-js');
const moment = require('moment');

const sleep = (timeout) => new Promise((resolve) => setTimeout(resolve, timeout));

const main = async () => {
    const kc = new k8s.KubeConfig();

    kc.loadFromCluster();

    const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

    let res = await k8sApi.listPersistentVolumeClaimForAllNamespaces();

    let pvcToVolumeName = {};
    res.body.items.forEach(_ => pvcToVolumeName[_.spec.volumeName] = _.metadata.name);

    console.log('volume name -> pvc', pvcToVolumeName);

    const volumeNames = Object.keys(pvcToVolumeName);

    const client = new DigitalOcean(process.env.DO_TOKEN);

    const toBackup = {};
    (await client.blockStorage.getAllBlockStorage())
        .filter(_ => volumeNames.includes(_.name))
        .forEach(_ => toBackup[_.name] = _.id);

    console.log('volume name -> id' , toBackup);

    const now = moment();

    const timestamp = now.format('YYYYMMDD');

    for (const name of Object.keys(toBackup)) {
        try {
            const id = toBackup[name];

            // cleanup
            const snapshots = await client.blockStorage.getSnapshotsForVolume(id);
            for (let snapshot of snapshots) {
                const createdAt = moment(snapshot.created_at);

                if (now.diff(createdAt, 'days') > 7) {
                    console.log(`Delete snapshot ${snapshot.name}`);
                    await client.snapshots.deleteSnapshot(snapshot.id)
                }
            }


            // backup
            const backupName = `backup-${timestamp}-${name}--${pvcToVolumeName[name].slice(0, 32)}`;
            console.log(id, backupName);
            await client.blockStorage.createSnapshotFromVolume(id, backupName);
            await sleep(11 * 60 * 1000);
        } catch (e) {
            console.log(e);
        }
    }
};

main();
